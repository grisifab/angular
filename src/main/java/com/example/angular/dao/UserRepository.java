package com.example.angular.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.angular.models.*;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
